<?php
define('_OK', 1);
require_once 'config.php';

session_start();
$token = $_SESSION['token'];

if ($_GET['token'] && $_GET['token'] == $token) {
    $pdf_path = env('pdf_path');
    $pdf_name = env('pdf_name');
    $file = $pdf_path . $pdf_name;
    // $file = $pdf_name;
    if (file_exists($file)) {
      
      header('Content-Description: File Transfer');
      header('Content-Type: application/force-download');
      header("Content-Disposition: attachment; filename=\"" . basename($pdf_name) . "\";");
      header('Content-Transfer-Encoding: binary');
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($file));
      ob_clean();
      flush();
      readfile($file); //showing the path to the server where the file is to be download
      
      unset($_SESSION["token"]);
      require_once 'thanks.view.php';
      exit;
    }
} else {
    header( "Location: index.php?info=809k" );
    exit;
}
