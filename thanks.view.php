<?php
// No direct access
defined('_OK') or die;
session_start();
$token = $_SESSION['token'];
$pdf_path = env('pdf_path');
$pdf_name = env('pdf_name');
$file = $pdf_path . $pdf_name;
?>
	<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
	<!DOCTYPE HTML>
	<html>

	<head>
		<title>The Power Model helps leaders build mental resilience | Leading Mindfully</title>
		<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
		<link rel="shortcut icon" href="images/fav.png" />
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/jquery.min.js"></script>
		<!---- start-smoth-scrolling---->
		<script type="text/javascript" src="js/move-top.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.validate.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function ($) {
				$(".scroll").click(function (event) {
					event.preventDefault();
					$('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
				});
			});
		</script>
		<!-- Custom Theme files -->
		<link href="css/style.css" rel='stylesheet' type='text/css' />
		<!-- Custom Theme files -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="application/x-javascript">
			addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); }
		</script>
		</script>
		<!----webfonts---->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700' rel='stylesheet' type='text/css'>
	</head>

	<body>
		<div class="header-section">
			<!----- start-header---->
			<div id="home" class="header">
				<div class="container">
					<div class="top-header">
						<div class="logo">
							<a href="#"><img src="images/logo.png" title="logo" /></a>
						</div>
						<nav class="top-nav">
							<ul class="top-nav">
								<li class="active"><a href="#contact" class="scroll">Download now! </a></li>
							</ul>
							<a href="#" id="pull"><img src="images/nav-icon.png" title="menu" /></a>
						</nav>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="">
					<div class="container" style="color:#eeffde;">
						<div class="col-md-8">
							<h1>Thank you!</span>
							</h1>
							<h2>The download will start in 3 seconds. If doesn't, please use this
								<a style="color:blue;" href="./<?= $pdf_name ?>">link to download the file</a>
							</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>

	</html>