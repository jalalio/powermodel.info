<?php

// No direct access
defined('_OK') or die;

$subject = env('email_subject');
$template = '<div style="padding:50px;">Hey, Shahla...!,<br/>'
. '<br/><br/>A visitor just submitted the form with the following information:<br/>'
. 'Name:' . filter_var( trim($_POST['name']), FILTER_SANITIZE_STRING ) . '<br/>'
. 'Email:' . filter_var( trim($_POST['email']), FILTER_SANITIZE_EMAIL ) . '<br/>'
. '<br/>'
. '</div>';
$sendmessage = "<div style=\"background-color:#fff; color:#000;\">" . $template . "</div>";
// Message lines should not exceed 70 characters (PHP rule), so wrap it.
$sendmessage = wordwrap($sendmessage, 70);

//Sendgrid API
require 'vendor/autoload.php';
$from = new SendGrid\Email(env('email_from_name'), env('email_from_email'));
$to = new \SendGrid\Email(env('email_to_name'), env('email_to_email'));
$content = new SendGrid\Content("text/html", $sendmessage);
$mail = new SendGrid\Mail($from, $subject, $to, $content);

$apiKey = env('apiKey');
$sg = new \SendGrid($apiKey);

$response = $sg->client->mail()->send()->post($mail);

if(!$response){
  header( "Location: index.php?info=MailError" );
  exit;
} else {
  session_start();
  $token = $_SESSION['token'];
  header( "Location: index.php?token={$token}&dispatched=1" );
  exit;
}
