<?php
//It rules
define('_OK', 1);
require_once 'config.php';

//Just start the show
session_start();
if(!$_SESSION["token"]){
  //Generate a random token for every request
  $token = rand();
  $_SESSION["token"] = $token;
} else {
  $token = $_SESSION["token"];
}
//If the form was submitted
if($_POST['name']){
  //Grab the input and send it out
  require_once 'mailer.php';
} else {
  //If the form was submitted and processed
  if($_GET['token'] && $_GET['token'] == $token){
    if($_GET['dispatched'] == '1'){
      require_once 'thanks.view.php';
      exit;
    } else {
      session_destroy();
    }
  }
  //If the form is not yet submitted
  require_once 'index.view.php';
  exit;
}
