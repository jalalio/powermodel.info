<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>

<head>
	<title>The Power Model helps leaders build mental resilience | Leading Mindfully</title>
	<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
	<link rel="shortcut icon" href="images/fav.png" />
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery.min.js"></script>
	<!---- start-smoth-scrolling---->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();
				$('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
			});
		});
	</script>
	<!-- Custom Theme files -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Custom Theme files -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="application/x-javascript">
		addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); }
	</script>
	</script>
	<!----webfonts---->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700' rel='stylesheet' type='text/css'>
	<!----//webfonts---->
	<!----start-top-nav-script---->
	<script>
		$(function () {
			var pull = $('#pull');
			menu = $('nav ul');
			menuHeight = menu.height();
			$(pull).on('click', function (e) {
				e.preventDefault();
				menu.slideToggle();
			});
			$(window).resize(function () {
				var w = $(window).width();
				if (w > 320 && menu.is(':hidden')) {
					menu.removeAttr('style');
				}
			});
		});
	</script>
	<!----//End-top-nav-script---->
</head>

<body>
	<!-----start-container----->
	<!-----header-section------>
	<div class="header-section">
		<!----- start-header---->
		<div id="home" class="header">
			<div class="container">
				<div class="top-header">
					<div class="logo">
						<a href="#"><img src="images/logo.png" title="logo" /></a>
					</div>
					<nav class="top-nav">
						<ul class="top-nav">
							<li class="active"><a href="#contact" class="scroll">Download now! </a></li>
						</ul>
						<a href="#" id="pull"><img src="images/nav-icon.png" title="menu" /></a>
					</nav>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="">
				<div class="container" style="color:#eeffde;">
					<div class="col-md-8">
						<h1>Introducing The <span>POWER Model</span></h1>
						<!-- <h2>The <strong>Power Model</strong> helps leaders build mental resilience</h2> -->
						<div>
							<p>
								The <strong>POWER Model</strong> offers the <strong>5 strategies</strong> required to build <strong>mental resilience</strong>. The key to developing
								mental resilience, which helps to cope with life stressors and bounce back from adversity, is practice. <strong>Self awareness</strong>
								(emotional intelligence language) and <strong>mindfulness</strong> (mindfulness language) is essentially the same thing, which contribute
								to <strong>your success</strong>. The <strong>POWER Model</strong> is an acronym for the <strong>5 practices</strong> you can start implementing into your routine,
								right away!
							</p>
						</div>
						<div class="">
							Here are some ways Leading Mindfully can affect your business outcome:
							<ul>
								<li><span> </span>Increase productivity and business results </li>
								<li><span> </span>Increase engagement and teamwork </li>
								<li><span> </span>Increase trust with leader</li>
								<li><span> </span>Decrease health care cost</li>
								<li><span> </span>Decrease turnoverDecrease turnover, absence & healthcare costs</li>
							</ul>
						</div>
					</div>
					<div class="erb-image-wrapper col-md-4">
						<img width="400" src="images/stones.png" title="" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="contact" class="getintouch">
		<div class="container">
			<div class="section-head text-center">
				<h3><span class="frist"> </span>Download Now!<span class="second"> </span></h3>
				<!-- <p>We respect your privacy. We will not share your information or spam you.</p> -->
			</div>
			<!---->
			<div class="col-md-8 getintouch-left">
				<div class="contact-form col-md-10">
					<form action="index.php" id="myform" method="post">
						<input type="text" placeholder="Name" required name="name" />
						<input type="text" placeholder="Email" required name="email" />
						<input type="submit" value="Get my copy!" />
					</form>
				</div>
				<ul class="footer-social-icons col-md-2 text-center">
					<li><a class="" href="https://www.instagram.com/leadingmindfully/">
								<i class="fa fa-instagram" aria-hidden="true"></i></a>
					</li>
					<li><a class="" href="https://twitter.com/StarlingBrook?lang=en">
								<i class="fa fa-twitter" aria-hidden="true"></i></a>
					</li>
					<li><a class="" href="https://www.facebook.com/leadingmindfullyunleashyourpower/">
								<i class="fa fa-facebook-square" aria-hidden="true"></i></a>
					</li>
					<li><a class="" href="https://www.linkedin.com/company-beta/24978440/admin/updates/">
								<i class="fa fa-linkedin" aria-hidden="true"></i></a>
					</li>
					<li><a class="" href="http://www.leadingmindfully.biz/">
								<i class="fa fa-globe" aria-hidden="true"></i></a>
					</li>
					<div class="clearfix"> </div>
				</ul>
			</div>
			<div class="col-md-4 getintouch-left">
				<div class="">
					<img width="300" class="erb-image-wrapper" src="images/ligthbulb.jpg" title="getintouch" />
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="container">
			<div class="footer-grids">
				<div class="col-md-9 footer-grid about-info">
					<a href="#"><img src="images/logo.png" title="Leading Mindfully" /></a>
					<p><small>Unleash your Power of Presence</small></p>
				</div>
				<div class="col-md-3 footer-grid subscribe">
					<p><a href="http://www.leadingmindfully.biz/">www.leadingmindfully.biz</a></p>
				</div>
				<!-- <div class="col-md-3 footer-grid explore">
							<div class="clearfix"> </div>
						</div> -->
				<div class="col-md-3 footer-grid copy-right">
					<p style="font-size:10px;color:#fff;" class="copy">Template by <a style="font-size:10px;color:#fff;" href="http://w3layouts.com/">w3layouts</a></p>
				</div>
				<div class="clearfix"> </div>
				<script type="text/javascript">
					$(document).ready(function () {
						/*
						var defaults = {
								containerID: 'toTop', // fading element id
							containerHoverID: 'toTopHover', // fading element hover id
							scrollSpeed: 1200,
							easingType: 'linear'
							};
						*/

						$().UItoTop({ easingType: 'easeOutQuart' });

					});
				</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
			</div>
		</div>
	</div>
</body>
</html>